var firstMovement;
var secondMovement;
var thirdMovement;
var fourthMovement;
var fifthMovement;
var innerMovement;
var innerBlocks;
var counter = 0;
var arrayNotes = new Array();
var treatGiven = false;
var showTarget = true;
var fade = 2000;
var codeVideo;
var infoVideo;
var deployVideo;
var allowIntro=true;
var timeOut1;
var timeOut2;

$(document).ready(function () {
	 $('#topBanner').slideDown(2000);
	 $('#bottomBanner').slideDown(2000, firstDiv);
});

function firstDiv() {
	if (allowIntro) {
		codeVideo = $('<video/>', {
			id: 'codeVideo',
			src: 'video/codeVideo.mp4',
			type: 'video/mp4',
			width: '100%',
			height: '100%',
			autoplay: true,
			controls: false,
		});
		codeVideo.appendTo($("#div1"));
		$("#div1").fadeIn(fade).delay(6000).fadeOut(fade);
		setTimeout(function () {
			secondDiv();
		}, 10000);
	}
}
function secondDiv() {
	if (allowIntro) {
	infoVideo = $('<video/>', {
		id: 'infoVideo',
		src: 'video/infoVideo.mp4',
		type: 'video/mp4',
		width: '100%',
		height: '100%',
		autoplay: true,
		controls: false,
	});
	infoVideo.appendTo($('#div2'));
	$("#div2").fadeIn(fade).delay(6000).fadeOut(fade);
	setTimeout(function () {
		thirdDiv();
	}, 10000);
}
}
function thirdDiv() {
	if (allowIntro) {
		deployVideo = $('<video/>', {
			id: 'deployVideo',
			src: 'video/deployVideo.mp4',
			type: 'video/mp4',
			width: '100%',
			height: '100%',
			autoplay: true,
			controls: false,
		});
		deployVideo.appendTo($("#div3"));
		$("#div3").fadeIn(fade).delay(5000).fadeOut(fade);
		setTimeout(function () {
			init();
		}, 7000);
	}
}

function init() {
	$('#topBanner').slideUp(2000);
	$('#bottomBanner').slideUp(2000);
	$('body').css("background-color", "white");
	$(function(){
	      $("#Popup").load("contact.html"); 
	    });
	document.getElementById("surpriseVideo").setAttribute("src",
			"https://www.youtube.com/embed/APFSsFBw6a0");
	var firstBlock = document.getElementById("firstBlock");
	firstBlock.style.position = 'relative';
	firstBlock.style.top = '-20cm';
	firstBlock.style.visibility = "visible";
	firstMovement = setInterval(function() {
		slideUpOne()
	}, 20);

	var secondBlock = document.getElementById("secondBlock");
	secondBlock.style.position = 'relative';
	secondBlock.style.top = '20cm';
	secondBlock.style.visibility = "visible";
	secondMovement = setInterval(function() {
		slideUpTwo()
	}, 15)
	var thirdBlock = document.getElementById("thirdBlock");
	thirdBlock.style.position = 'relative';
	thirdBlock.style.top = '-13.5cm'
	thirdBlock.style.left = '-30cm';
	thirdBlock.style.visibility = "visible";
	thirdMovement = setInterval(function() {
		slideUpThree()
	}, 10);
	var fourthBlock = document.getElementById("fourthBlock");
	fourthBlock.style.position = 'relative';
	fourthBlock.style.right = '-35cm';
	fourthBlock.style.top = '-15.3cm';
	fourthBlock.style.visibility = "visible";
	setTimeout(runFour, 1750);

	innerBlocks = document.getElementsByClassName("innerImg");
	for (var a = 0; a < innerBlocks.length; a++) {
		innerBlocks[a].style.opacity = 1;
	}
	innerMovement = setInterval(function() {
		lessOpaque()
	}, 200);

	setTimeout(initBlocks, 3500);

	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i
			.test(navigator.userAgent)) {
		showTarget = false;
		document.getElementById("dragTarget").style.visibility = "hidden";
		document.getElementById("pianoSection").style.width = "60%";
		document.getElementById("pianoSection").style.height = "30%";
		fourthBlock.onclick = function() {

			fourthBlock.style.visibility = "hidden";
			$('#showValue').css('visibility', 'hidden');
			pianoFade();
			changeIntoKeys();
			movePianoUp();
			document.getElementById("logoSection").removeChild(firstBlock);
			document.getElementById("logoSection").removeChild(secondBlock);
			document.getElementById("logoSection").removeChild(thirdBlock);
			document.getElementById("Fsharp").setAttribute("onclick",
					"sound(event)");
			document.getElementById("Fsharp").style.backgroundColor = "black";
			document.getElementById("Gsharp").setAttribute("onclick",
					"sound(event)");
			document.getElementById("Gsharp").style.backgroundColor = "black";
			document.getElementById("Asharp").setAttribute("onclick",
					"sound(event)");
			document.getElementById("Asharp").style.backgroundColor = "black";

		}
	}

	if (false || !!document.documentMode) {
		window
				.alert("For best user experience, we recommend Google Chrome or Mozilla Firefox")
	}
}

function initBlocks() {
	var nameElement = document.getElementById("showValue");
	firstBlock.onmouseover = function() {
		innerBlocks[0].style.opacity = 1;
		nameElement.style.visibility = "visible";
		nameElement.innerHTML = firstBlock.title;
	}
	firstBlock.onmouseout = function() {
		innerBlocks[0].style.opacity = 0;
		nameElement.style.visibility = "hidden";
	}
	secondBlock.onmouseover = function() {
		innerBlocks[1].style.opacity = 1;
		nameElement.style.visibility = "visible";
		nameElement.innerHTML = secondBlock.title;
	}
	secondBlock.onmouseout = function() {
		innerBlocks[1].style.opacity = 0;
		nameElement.style.visibility = "hidden";
	}

	thirdBlock.onmouseover = function() {
		innerBlocks[2].style.opacity = 1;
		nameElement.style.visibility = "visible";
		nameElement.innerHTML = thirdBlock.title;
	}
	thirdBlock.onmouseout = function() {
		innerBlocks[2].style.opacity = 0;
		nameElement.style.visibility = "hidden";
	}

	fourthBlock.onmouseover = function() {
		if (showTarget) {
			revealTarget();
		}
	}
	fourthBlock.onmouseout = function() {
		hideTarget();
	}
}

function lessOpaque() {
	for (var a = 0; a < innerBlocks.length; a++) {
		innerBlocks[a].style.opacity = parseFloat(innerBlocks[a].style.opacity) - 0.06;
	}
	if (innerBlocks[2].style.opacity < 0) {
		clearInterval(innerMovement);
	}
}

function runFour() {
	fourthMovement = setInterval(function() {
		slideUpFour()
	}, 1);
}

function slideUpOne() {
	firstBlock.style.top = parseFloat(firstBlock.style.top) + 0.15 + 'cm';
	if (firstBlock.style.top == "2cm" || parseFloat(firstBlock.style.top) > 2) {
		clearInterval(firstMovement);
	}

}

function slideUpTwo() {
	secondBlock.style.top = parseFloat(secondBlock.style.top) - 0.15 + 'cm';
	if (secondBlock.style.top == "-6.7cm"
			|| parseFloat(secondBlock.style.top) < -6.7) {
		clearInterval(secondMovement);
	}

}
function slideUpThree() {
	thirdBlock.style.left = parseFloat(thirdBlock.style.left) + 0.1 + 'cm';
	if (thirdBlock.style.left == "6.5cm"
			|| parseFloat(thirdBlock.style.left) > 6.5) {
		clearInterval(thirdMovement);
	}

}
function slideUpFour() {
	fourthBlock.style.right = parseFloat(fourthBlock.style.right) + 0.1 + 'cm';
	if (fourthBlock.style.right == "5cm"
			|| parseFloat(fourthBlock.style.right) > 5) {
		clearInterval(fourthMovement);
		fifthMovement = setInterval(function() {
			slideUpFive()
		}, 10);
	}

}
function slideUpFive() {
	fourthBlock.style.right = parseFloat(fourthBlock.style.right) - 0.1 + 'cm';
	if (fourthBlock.style.right == "-7.5cm"
			|| parseFloat(fourthBlock.style.right) < -7.6) {
		clearInterval(fifthMovement);

	}
}
function revealTarget() {
	document.getElementById("dragTarget").style.visibility = "visible";
}
function hideTarget() {
	document.getElementById("dragTarget").style.visibility = "hidden";
}

function drop(ev) {
	ev.preventDefault();
	var data = ev.dataTransfer.getData("text");
	ev.target.appendChild(document.getElementById(data));
	document.getElementById("dragTarget").style.visibility = "hidden";
	$('#showValue').css('visibility','hidden');
	pianoFade();
	changeIntoKeys();

}

function pianoFade() {
	$("#pianoSection").fadeIn("slow");
}

function dropKey(ev) {
	var id2 = ev.dataTransfer.getData("id2");
	var id1 = ev.dataTransfer.getData("id1");
	if (ev.target.id == id2) {
		ev.preventDefault();
		document.getElementById("logoSection").removeChild(
				document.getElementById(id1));
		var target = document.getElementById(ev.target.id);
		target.style.backgroundColor = "black";
		target.setAttribute("onclick", "sound(event)");
		counter++;
	}
	if (counter == 3) {
		movePianoUp();
	}
}

function movePianoUp() {
	document.getElementById("pianoSection").style.transition = "10s";
	document.getElementById("pianoSection").style.bottom = "50%";

	timeOut1=setTimeout(reveal, 5000);
	timeOut2=setTimeout(changeText, 25000);
	function reveal() {
		document.getElementById("showValue").innerHTML = "Now all you need to find is the right combination!"
		$('#showValue').css('visibility', 'visible');
	}
	function changeText() {
		document.getElementById("showValue").innerHTML = "Give up? Click here for some help"
		document.getElementById("showValue").onclick = changeColour;
	}

}

function changeColour() {
	document.getElementById("showValue").innerHTML = "But in what order? Start with the D#"
	document.getElementById("D").style.backgroundColor = "green";
	document.getElementById("Dsharp").style.backgroundColor = "green";
	document.getElementById("Fsharp").style.backgroundColor = "green";
	document.getElementById("C5").style.backgroundColor = "green";
	document.getElementById("showValue").onclick = null;

}

function unChangeColour() {
	document.getElementById("D").style.backgroundColor = "white";
	document.getElementById("Dsharp").style.backgroundColor = "black";
	document.getElementById("Fsharp").style.backgroundColor = "black";
	document.getElementById("C5").style.backgroundColor = "white";
}

function dragKey(ev) {
	ev.dataTransfer.setData("id2", ev.target.getAttribute("id2"));
	ev.dataTransfer.setData("id1", ev.target.id);
}

function drag(ev) {
	ev.dataTransfer.setData("text", ev.target.id);

}

function allowDrop(ev) {
	ev.preventDefault();
}

function changeIntoKeys() {
	var firstBlock = document.getElementById("firstBlock");
	var secondBlock = document.getElementById("secondBlock");
	var thirdBlock = document.getElementById("thirdBlock");
	firstBlock.onmouseout = null;
	firstBlock.onmouseover = null;
	firstBlock.setAttribute("draggable", "true");
	firstBlock.style.transition = "5s";
	firstBlock.style.transform = "rotate(0deg)";
	firstBlock.removeChild(firstBlock.firstElementChild);
	firstBlock.style.top = "0cm";
	firstBlock.style.left = "1.5cm";
	firstBlock.style.width = "1cm";
	firstBlock.style.height = "3.8cm";
    firstBlock.style.position="absolute"
	secondBlock.onmouseout = null;
	secondBlock.onmouseover = null;
	secondBlock.setAttribute("draggable", "true");
	secondBlock.style.transform = "rotate(0deg)";
	secondBlock.style.top = "-1cm";
	secondBlock.style.left = "0m";
	secondBlock.style.width = "1cm";
	secondBlock.style.height = "3.8cm";
	secondBlock.style.transition = "3s";
	secondBlock.removeChild(secondBlock.firstElementChild);
    secondBlock.style.position="absolute"
	thirdBlock.onmouseout = null;
	thirdBlock.onmouseover = null;
	thirdBlock.setAttribute("draggable", "true");
	thirdBlock.style.transform = "rotate(0deg)";
	thirdBlock.removeChild(thirdBlock.firstElementChild);
	thirdBlock.style.top = "-2cm";
	thirdBlock.style.width = "1cm";
	thirdBlock.style.height = "3.8cm";
	thirdBlock.style.transition = "3s";
    firstBlock.style.position="absolute"
}

function sound(ev) {
	var note = document.getElementById("audio" + ev.target.id);
	note.play();
	if (!treatGiven) {
		var notePlayed = ev.target.id;
		arrayNotes.push(notePlayed);
		var arrayNotesString = arrayNotes.toString();
		if (arrayNotesString.includes("Dsharp,C5,Fsharp,D")) {
			treatGiven = true;
			$('#showValue').css('visibility','hidden');
			setTimeout(surpriseVideo, 10);
		}
	}
}

function soundSpecial(ev) {
	var note = document.getElementById("audio" + ev.target.getAttribute("id2"));
	note.play();
}

function surpriseVideo() {
	clearTimeout(timeOut1);
	clearTimeout(timeOut2);
	$('#topBanner').slideDown(2000);
	$('#skipImage').css('display','none');
	$('#bottomBanner').slideDown(2000);
	$('body').css("background-color", "lightgrey");
	$("#pianoSection").fadeOut("fast");
	$("#surpriseSection").fadeIn();
	$("#surpriseSection").animate({
		left : '50%'
	});
	document.getElementById("surpriseVideo").setAttribute("src",
			"https://www.youtube.com/embed/APFSsFBw6a0?autoplay=1");
	setTimeout(endVideo, 115000);
}

function endVideo() {
	$("#pianoSection").fadeIn("normal");
	unChangeColour();
	$("#surpriseSection").animate({
		left : '-150%'
	});
	document.getElementById("surpriseVideo").setAttribute("src",
			"https://www.youtube.com/embed/APFSsFBw6a0?autoplay=0");
	$('#topBanner').slideUp(2000);
	$('#bottomBanner').slideUp(2000);
	$('body').css("background-color", "white");
	$('#skipImage').css('display','block');
	$('#showValue').css('visibility','visible');
	clearTimeout(timeOut1);
	clearTimeout(timeOut2);
	document.getElementById("showValue").innerHTML = "Well! Miss Montero sure did struck a key there, didn't she? Press here to return to the homepage."
	document.getElementById("showValue").onclick = function() {
		window.location.reload(false)
	};
}

(function(i, s, o, g, r, a, m) {
	i['GoogleAnalyticsObject'] = r;
	i[r] = i[r] || function() {
		(i[r].q = i[r].q || []).push(arguments)
	}, i[r].l = 1 * new Date();
	a = s.createElement(o), m = s.getElementsByTagName(o)[0];
	a.async = 1;
	a.src = g;
	m.parentNode.insertBefore(a, m)
})(window, document, 'script', 'https://www.google-analytics.com/analytics.js',
		'ga');

ga('create', 'UA-92126488-1', 'auto');
ga('send', 'pageview');



// Function To Display Popup
function div_show() {
	document.getElementById('Popup').style.display = "block";
}
// Function to Hide Popup
function div_hide() {
	var a = document.getElementById("name");
	a.value = a.defaultValue;
	var b = document.getElementById("email");
	b.value = a.defaultValue;
	var c = document.getElementById("msg");
	c.value = a.defaultValue;
	var c = document.getElementById("subject");
	c.value = a.defaultValue;
	document.getElementById('Popup').style.display = "none";
}


//Function to skip intro
function skipIntro(){
	$('#introSection').fadeOut();
	allowIntro=false;
	init();


}