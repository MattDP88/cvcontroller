var form;
var data;

function check_empty(e) {
	if (document.getElementById('name').value == ""
			|| document.getElementById('email').value == ""
			|| document.getElementById('msg').value == ""
			|| document.getElementById('subject').value =="") {
		alert("Fill All Fields !");
	} else {
		handleForm(e);
	}
}
function handleForm(e) {
	form = document.getElementById("contactForm");
	e.preventDefault();
	data = {};
	for (var i = 0, ii = form.length; i < ii; ++i) {
		var input = form[i];
		if (input.name) {
			data[input.name] = input.value;
		}
	}
	addData();
}
function addData() {
	$
			.ajax({
				type : "POST",
				url : "https://matthiasdepoorter.herokuapp.com/Rest/Form",
				data : JSON.stringify(data),
				contentType : "application/json",
				crossDomain : true,
				dataType : "json",
				success : function() {

					alert('Thank you for your message. I will contact you as soon as possible.');
					div_hide();
				},
				error : function(status) {
					alert('Something went wrong. Please try again later.');
					div_hide();
				}
			});
}