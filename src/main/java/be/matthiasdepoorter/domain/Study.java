package be.matthiasdepoorter.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table
public class Study implements Serializable{
	@Id
	@Column(name="PK", unique=true, updatable=true)
	private String userName;
	
	@OneToOne (cascade={CascadeType.ALL}, fetch=FetchType.EAGER)
	private Degree degree;
	
	@ManyToOne
	@JoinColumn(name="jobsearcherFK")
	private Applicant jobsearcher1;
	
	public Study(){
		
	}


	public String getName() {
		return userName;
	}


	public void setName(String name) {
		this.userName = name;
	}


	public Degree getDegree() {
		return degree;
	}


	public void setDegree(Degree degree) {
		this.degree = degree;
	}


	public Applicant getJobsearcher() {
		return jobsearcher1;
	}

	@XmlTransient
	@JsonIgnore
	public void setJobsearcher(Applicant jobsearcher) {
		this.jobsearcher1 = jobsearcher;
	}
	
	
}
