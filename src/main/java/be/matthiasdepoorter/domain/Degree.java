package be.matthiasdepoorter.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.ColumnDefault;

@Entity
@Table
public class Degree implements Serializable {

	@Id
	@Column(name = "PK", unique = true)
	private String PK;

	@Column(name = "title")
	private String title;

	@Column(name = "location")
	private String location;

	@Column(name = "dateReceived")
	private String dateReceived;

	@Column(name = "dates")
	private String dates;

	@Column(name = "certificates")
	private String certificates;

	public Degree() {

	}

	public String getPK() {
		return PK;
	}

	public void setPK(String pK) {
		PK = pK;
	}

	public String getTitle() {
		return (title.contains("\t") ? "" : "\t") + title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLocation() {
		return (location.contains("\n\t\t") ? "" : "\n\t\t") + location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDateReceived() {
		return (!StringUtils.isBlank(dateReceived)
				? (dateReceived.contains("(") ? dateReceived : "(" + dateReceived + ")") : "");
	}

	public void setDateReceived(String dateReceived) {
		this.dateReceived = dateReceived;
	}

	public String getDates() {
		return dates;
	}

	public void setDates(String dates) {
		this.dates = dates;
	}

	public String getCertificates() {
		return (!StringUtils.isBlank(certificates)
				? (certificates.contains("\n\t\t") ? certificates : "\n\t\t" + certificates) : "");
	}

	public void setCertificates(String certificates) {
		this.certificates = certificates;
	}

	@Override
	public String toString() {
		return "Degree [title=" + title + ", location=" + location + ", dateReceived=" + dateReceived + ", dates="
				+ dates + "]";
	}

}
