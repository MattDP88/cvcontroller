package be.matthiasdepoorter.domain;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table
public class Work implements Serializable, Comparable<Work> {

	@Id
	@Column(name = "PK", unique = true)
	private String PK;

	@Column(name = "title")
	private String title;

	@Column(name = "company")
	private String company;

	@Column(name = "startDate")
	private Date startDate;

	@Column(name = "endDate")
	private Date endDate;

	@Column(name = "relevant")
	private boolean relevant;

	@Column(name = "information", length = 2500)
	private String information;

	@ManyToOne
	private Applicant jobsearcher;

	public Work() {
	}

	public String getPK() {
		return PK;
	}

	public void setPK(String pK) {
		PK = pK;
	}

	public String getTitle() {
		return (title.contains("\t") ? "" : "\t") + title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCompany() {
		return (company.contains("\n\t\t") ? "" : "\n\t\t") + company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Applicant getJobsearcher() {
		return jobsearcher;
	}

	@XmlTransient
	@JsonIgnore
	public void setJobsearcher(Applicant jobsearcher) {
		this.jobsearcher = jobsearcher;
	}

	public boolean isRelevant() {
		return relevant;
	}

	public void setRelevant(boolean relevant) {
		this.relevant = relevant;
	}

	public String getInformation() {
		if (!StringUtils.isBlank(information)) {
			return "\n\t\t" + information;
		} else {
			return "";
		}
	}

	public void setInformation(String information) {
		this.information = information;
	}

	@Override
	public String toString() {
		return "Work [title=" + title + ", company=" + company + ", dates=" + startDate + endDate + "]";
	}

	@Override
	public int compareTo(Work o) {
		return o.endDate.compareTo(this.endDate) + o.startDate.compareTo(this.startDate);
	}
}
