package be.matthiasdepoorter.domain;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table
public class Applicant implements Serializable{
	@Id
	@Column(name="userName", unique=true)
	private String userName;
	
	@Column(name="address")
	private String address;
	
	@Column(name="birthday")
	private String birthday;
	
	@Column(name="email")
	private String email;
	
	@Column(name="phone")
	private String phone;
	
	@Column (name="website")
	private String website;
	
	@Version
	private int version;
	
	@Column (name="profile", length=2500)
	private String profile;
	
	@OneToMany(mappedBy="jobsearcher1", cascade={CascadeType.ALL}, fetch=FetchType.LAZY)
	Collection <Study> studies;
	
	@OneToMany(mappedBy="jobsearcher", cascade={CascadeType.ALL}, fetch=FetchType.LAZY)
	Collection <Work> workExperiences;
	
	@Column (name="skills")
	@Lob
	private byte[] skills;

	public String getName() {
		return userName + "\n";
	}

	public void setName(String name) {
		this.userName = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBirthday() {
		return birthday +(birthday.contains("\n")?"":"\n");
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getEmail() {
		return email +(email.contains("\n")?"":"\n");
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone +(phone.contains("\n")?"":"\n");
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getWebsite() {
		return website +(website.contains("\n")?"":"\n");
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public Collection<Study> getStudies() {
		return studies;
	}

	public void setStudies(Collection<Study> studies) {
		this.studies = studies;
	}

	public Collection<Work> getWorkExperiences() {
		return workExperiences;
	}

	public void setWorkExperiences(Collection<Work> workExperiences) {
		this.workExperiences = workExperiences;
	}

	public byte[] getSkills() {
		return skills;
	}

	public void setSkills(byte[] skills) {
		this.skills = skills;
	}
	

	public String getProfile() {
		return (profile.contains("\n\n")?"":"\n\n")+profile+(profile.contains("\n")?"":"\n");
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}
	
	

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "JobSearcher [name=" + userName + ", address=" + address + ", birthday=" + birthday + ", email=" + email
				+ ", phone=" + phone + ", website=" + website + ", profile=" + profile +"]";
	}

	

	
	
	

}
