package be.matthiasdepoorter.controller;

import org.apache.catalina.connector.Response;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import be.matthiasdepoorter.ControllerApp;
import be.matthiasdepoorter.domain.Applicant;
import be.matthiasdepoorter.domain.Contact;
import be.matthiasdepoorter.repositories.interfaces.ApplicantRepository;
import be.matthiasdepoorter.utilities.SendHTMLEmail;

@RestController
@RequestMapping("/Rest")
public class ApplicantController {

	private ApplicantRepository repo;

	@Autowired
	public void setApplicantRepository(ApplicantRepository repo) {
		this.repo = repo;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public Applicant getApplicant(@PathVariable("id") String id) {
		Applicant j = repo.getApplicant(id);
		return j;
	}

	@RequestMapping(value = "/update/{access}", method = RequestMethod.PUT)
	public void updateApplicant(@RequestBody Applicant applicant, @PathVariable ("access") int access) {
		if (access==45627){
		applicant.getStudies().forEach(s->s.setJobsearcher(applicant));
		applicant.getWorkExperiences().forEach(w->w.setJobsearcher(applicant));
		repo.updateApplicant(applicant);
		}
	}

	@RequestMapping(value = "/delete/{access}/{id}", method = RequestMethod.DELETE)
	public void deleteApplicant(@PathVariable("id") String id, @PathVariable ("access") int access) {
		if (access==72564){
		repo.deleteApplicant(id);
		}
	}

	@RequestMapping(method = RequestMethod.POST, consumes={"application/json"})
	public void createApplicant(@RequestBody Applicant applicant) {
		applicant.getStudies().forEach(s->s.setJobsearcher(applicant));
		applicant.getWorkExperiences().forEach(w->w.setJobsearcher(applicant));
		repo.createApplicant(applicant);
	}
	
	@CrossOrigin
	@RequestMapping(value="/Form",method=RequestMethod.POST)
	public int handleForm(@RequestBody Contact contact){
		try{
		new SendHTMLEmail().SendSimple(contact);
		} catch (Exception e){
			Logger logger = Logger.getLogger(ControllerApp.class);
		logger.error(e.getMessage());
		}
		return Response.SC_OK;
	}

}
