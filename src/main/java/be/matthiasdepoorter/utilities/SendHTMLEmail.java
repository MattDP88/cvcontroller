package be.matthiasdepoorter.utilities;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientResponse;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

import be.matthiasdepoorter.domain.Contact;

public class SendHTMLEmail {

	public ClientResponse SendSimple(Contact contact) {

		Client client = ClientBuilder.newClient();
		client.register(HttpAuthenticationFeature.basic("api", "key-9c206a57f2f9ec537d76f5e776aa6168"));

		WebTarget mgRoot = client.target("https://api.mailgun.net/v2");

		Form reqData = new Form();
		reqData.param("from", "Matthias@matthiasdepoorter.herokuapp.com");
		reqData.param("to", "matthiasj.depoorter@gmail.com");
		reqData.param("subject", "Contact from website: " + contact.getSubject());
		reqData.param("text", "Original message: " + contact.getMessage() + "\nName: " + contact.getName()
				+ "\nEmail: " + contact.getEmail());

		return mgRoot.path("/{domain}/messages")
				.resolveTemplate("domain", "app19e2032b371a493985a56edc9b057536.mailgun.org")
				.request(MediaType.APPLICATION_FORM_URLENCODED)
				.buildPost(Entity.entity(reqData, MediaType.APPLICATION_FORM_URLENCODED)).invoke(ClientResponse.class);

	}

}
