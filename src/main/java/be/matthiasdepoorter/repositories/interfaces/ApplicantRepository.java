package be.matthiasdepoorter.repositories.interfaces;


import be.matthiasdepoorter.domain.Applicant;

public interface ApplicantRepository{
	
	public void createApplicant(Applicant applicant);

	public Applicant getApplicant(String id);
	
	public void deleteApplicant(String id);

	public void updateApplicant (Applicant applicant);
	

}
