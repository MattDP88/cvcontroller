package be.matthiasdepoorter.repositories.implementations;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import be.matthiasdepoorter.domain.Applicant;
import be.matthiasdepoorter.repositories.interfaces.ApplicantRepository;
@Transactional
@Service
@EntityScan("be.matthiasdepoorter.domain")
public class ApplicantRepositoryImpl implements ApplicantRepository {

	private EntityManager em;

	@PersistenceContext
	public void setEntityManger(EntityManager em) {
		this.em = em;
	}

	@Override
	public void createApplicant(Applicant applicant) {
		em.persist(applicant);
		
	}

	@Override
	public Applicant getApplicant(String id) {
		Applicant applicant = em.find(Applicant.class, id);
		return applicant;
	}

	@Override
	public void deleteApplicant(String id) {
		em.remove(em.find(Applicant.class, id));
		
	}
	
	@Override
	public void updateApplicant (Applicant applicant){
		Applicant b = em.find(Applicant.class, applicant.getName());
		em.lock(b, LockModeType.OPTIMISTIC);
		em.merge(applicant);

		
	}



}
