package be.matthiasdepoorter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "be.matthiasdepoorter")
public class ControllerApp {

	public static void main(String[] args) {

		SpringApplication.run(ControllerApp.class, args);
	}
}
